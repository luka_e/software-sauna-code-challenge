package com.codilium.pathfinder.helper;

import com.codilium.pathfinder.enums.Direction;
import com.codilium.pathfinder.exception.BrokenPathException;
import com.codilium.pathfinder.model.Node;

public class DirectionHelper {

    public Direction findInitialDirection(Node initialNode) {
        Direction direction = null;
        if(initialNode.getRight() != null) {
            direction = Direction.RIGHT;
        } else if (initialNode.getLeft() != null) {
            direction = Direction.LEFT;
        } else if (initialNode.getUp() != null) {
            direction = Direction.UP;
        } else if (initialNode.getDown() != null) {
            direction = Direction.DOWN;
        } else {
            throw  new BrokenPathException();
        }
        return direction;
    }

    public Direction determineDirection(Node node, Direction direction) {
        switch (direction) {
            case UP:
            case DOWN:
                if (node.getLeft() != null) {
                    return Direction.LEFT;
                } else if (node.getRight() != null) {
                    return Direction.RIGHT;
                }
                break;
            case RIGHT:
            case LEFT:
                if (node.getUp() != null) {
                    return Direction.UP;
                } else if (node.getDown() != null) {
                    return Direction.DOWN;
                }
                break;
        }
        return direction;
    }
}
