package com.codilium.pathfinder.helper;

import com.codilium.pathfinder.exception.*;
import com.codilium.pathfinder.model.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NodeHelper {

    public void validateTurn(Node node) {
        List<Node> connectedNodes = new ArrayList<>();
        if (node.getUp() != null && node.getUp().getValue() != '-') {
            connectedNodes.add(node.getUp());
        }
        if (node.getDown() != null && node.getDown().getValue() != '-') {
            connectedNodes.add(node.getDown());
        }
        if (node.getLeft() != null && node.getLeft().getValue() != '|') {
            connectedNodes.add(node.getLeft());
        }
        if (node.getRight() != null && node.getRight().getValue() != '|') {
            connectedNodes.add(node.getRight());
        }
        if (connectedNodes.size() > 2) {
            throw new TForkException();
        }
    }

    public boolean isLetter(Node node) {
        return node.getValue() >= 'A' && node.getValue() <= 'Z';
    }

    public Node findStart(List<Node> nodes) {
        List<Node> starts = nodes.stream().filter(node -> node.getValue() == '@').collect(Collectors.toList());
        if(starts.isEmpty()) {
            throw new NoStartException();
        }
        if(starts.size() > 1) {
            throw new MultipleStartsException();
        }
        return starts.get(0);
    }

    public void validateEnd(List<Node> nodes) {
        long endCount = nodes.stream().filter(node -> node.getValue() == 'x').count();
        if(endCount == 0) {
            throw new NoEndException();
        }
        if(endCount > 1) {
            throw new MultipleEndsException();
        }
    }
}
