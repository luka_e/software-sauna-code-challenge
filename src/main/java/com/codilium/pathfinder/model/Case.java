package com.codilium.pathfinder.model;

import java.util.List;

public class Case {
    private final List<Node> nodes;
    private final String resultPath;
    private final String resultLetters;
    private final String error;

    public Case(List<Node> nodes, String resultPath, String resultLetters, String error) {
        this.nodes = nodes;
        this.resultPath = resultPath;
        this.resultLetters = resultLetters;
        this.error = error;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public String getResultPath() {
        return resultPath;
    }

    public String getResultLetters() {
        return resultLetters;
    }

    public String getError() {
        return error;
    }
}
