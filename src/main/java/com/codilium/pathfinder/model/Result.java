package com.codilium.pathfinder.model;

public class Result {
    private final String path;
    private final String letters;
    private final String error;

    public Result(String path, String letters) {
        this.path = path;
        this.letters = letters;
        this.error = null;
    }

    public Result(String error) {
        this.path = null;
        this.letters = null;
        this.error = error;
    }

    public String getPath() {
        return path;
    }

    public String getLetters() {
        return letters;
    }

    public String getError() {
        return error;
    }
}
