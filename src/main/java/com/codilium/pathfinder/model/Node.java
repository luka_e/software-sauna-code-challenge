package com.codilium.pathfinder.model;

import com.codilium.pathfinder.enums.Direction;

public class Node {
    private Node up;
    private Node down;
    private Node left;
    private Node right;
    private final char value;
    private final int x;
    private final int y;

    public Node(char value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public Node getNodeForDirection(Direction direction) {
        switch (direction) {
            case UP:
                return up;
            case DOWN:
                return down;
            case LEFT:
                return left;
            case RIGHT:
                return right;
            default:
                return null;
        }
    }

    public Node getUp() {
        return up;
    }

    public void setUp(Node up) {
        this.up = up;
    }

    public Node getDown() {
        return down;
    }

    public void setDown(Node down) {
        this.down = down;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public char getValue() {
        return value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
