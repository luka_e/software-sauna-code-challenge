package com.codilium.pathfinder.exception;

public class NoEndException extends RuntimeException{
    public NoEndException() {
        super("No end");
    }
}
