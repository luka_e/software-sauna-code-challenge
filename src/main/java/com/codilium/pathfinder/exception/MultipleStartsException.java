package com.codilium.pathfinder.exception;

public class MultipleStartsException extends RuntimeException{
    public MultipleStartsException() {
        super("Multiple starts");
    }
}
