package com.codilium.pathfinder.exception;

public class BrokenPathException extends RuntimeException {
    public BrokenPathException() {
        super("Broken path");
    }
}
