package com.codilium.pathfinder.exception;

public class NoStartException extends RuntimeException{
    public NoStartException() {
        super("No start");
    }
}
