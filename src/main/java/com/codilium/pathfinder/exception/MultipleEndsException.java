package com.codilium.pathfinder.exception;

public class MultipleEndsException extends RuntimeException{
    public MultipleEndsException() {
        super("Multiple ends");
    }
}
