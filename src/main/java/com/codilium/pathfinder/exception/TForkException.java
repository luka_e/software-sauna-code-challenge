package com.codilium.pathfinder.exception;

public class TForkException extends RuntimeException{
    public TForkException() {
        super("T fork");
    }
}
