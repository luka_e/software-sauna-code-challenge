package com.codilium.pathfinder;

import com.codilium.pathfinder.helper.DirectionHelper;
import com.codilium.pathfinder.helper.NodeHelper;
import com.codilium.pathfinder.model.Case;
import com.codilium.pathfinder.model.Node;
import com.codilium.pathfinder.model.Result;

import java.util.List;

public class Main {

    public static void main(String... args) {
        for(Case caze : Inputs.cases) {
            try {
                List<Node> nodes = caze.getNodes();
                Result result = new NodeTraveler(new NodeHelper(), new DirectionHelper()).travelNodes(nodes);
                displayResult(result);
            } catch (Exception e) {
                displayResult(new Result(e.getMessage()));
            }
        }
    }

    private static void displayResult(Result result) {
        if(result.getError() != null) {
            System.out.println("Error: " + result.getError());
        } else {
            System.out.println(result.getPath());
            System.out.println(result.getLetters() + "\n");
        }
    }
}
