package com.codilium.pathfinder;

import com.codilium.pathfinder.enums.Direction;
import com.codilium.pathfinder.exception.BrokenPathException;
import com.codilium.pathfinder.helper.DirectionHelper;
import com.codilium.pathfinder.helper.NodeHelper;
import com.codilium.pathfinder.model.Node;
import com.codilium.pathfinder.model.Result;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class NodeTraveler {

    private final NodeHelper nodeHelper;
    private final DirectionHelper directionHelper;

    public NodeTraveler(NodeHelper nodeHelper, DirectionHelper directionHelper) {
        this.nodeHelper = nodeHelper;
        this.directionHelper = directionHelper;
    }

    public Result travelNodes(List<Node> nodes) {
        Node initialNode = nodeHelper.findStart(nodes);

        Direction direction = directionHelper.findInitialDirection(initialNode);

        List<Node> path = new ArrayList<>();
        travelNodesRecursive(initialNode, direction, path);

        nodeHelper.validateEnd(nodes);
        return buildResult(path);
    }

    private void travelNodesRecursive(Node currentNode, Direction direction, List<Node> path) {
        path.add(currentNode);

        if(currentNode.getValue() == 'x') {
            return;
        }
        Direction nextDirection = direction;
        if(currentNode.getValue() == '+') {
            this.nodeHelper.validateTurn(currentNode);
            nextDirection = this.directionHelper.determineDirection(currentNode, direction);
        }
        Node nextNode = currentNode.getNodeForDirection(nextDirection);
        if(nextNode != null) {
            travelNodesRecursive(nextNode, nextDirection, path);
        } else if(nodeHelper.isLetter(currentNode)){
            nextDirection = this.directionHelper.determineDirection(currentNode, direction);
            nextNode = currentNode.getNodeForDirection(nextDirection);
            if(nextNode != null) {
                travelNodesRecursive(nextNode, nextDirection, path);
            }
        } else {
            throw new BrokenPathException();
        }
    }

    private Result buildResult(List<Node> path) {
        StringBuilder pathString = new StringBuilder();
        Set<Node> letterNodes = new LinkedHashSet<>();
        for (Node node : path) {
            pathString.append(node.getValue());
            if(this.nodeHelper.isLetter(node)) {
                letterNodes.add(node);
            }
        }
        StringBuilder letterString = new StringBuilder();
        for(Node node : letterNodes) {
            letterString.append(node.getValue());
        }
        return new Result(pathString.toString(), letterString.toString());
    }
}
