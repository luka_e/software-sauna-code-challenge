package com.codilium.pathfinder.enums;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
