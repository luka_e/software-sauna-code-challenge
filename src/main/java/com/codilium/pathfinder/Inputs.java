package com.codilium.pathfinder;

import com.codilium.pathfinder.model.Case;
import com.codilium.pathfinder.model.Node;

import java.util.ArrayList;
import java.util.List;

public class Inputs {

    private Inputs() {
    }

    static List<Case> cases = new ArrayList<>();

    static {
        cases.add(
                new Case(
                        convertToNodes(
                                "  @---A---+\n" +
                                        "          |\n" +
                                        "  x-B-+   C\n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        "@---A---+|C|+---+|+-B-x",
                        "ACB",
                        null));

        cases.add(
                new Case(
                        convertToNodes(
                                "  @\n" +
                                        "  | +-C--+\n" +
                                        "  A |    |\n" +
                                        "  +---B--+\n" +
                                        "    |      x\n" +
                                        "    |      |\n" +
                                        "    +---D--+"),
                        "@|A+---B--+|+--C-+|-||+---D--+|x",
                        "ABCD",
                        null));


        cases.add(
                new Case(
                        convertToNodes(
                                "  @---A---+\n" +
                                        "          |\n" +
                                        "  x-B-+   |\n" +
                                        "      |   |\n" +
                                        "      +---C"),
                        "@---A---+|||C---+|+-B-x",
                        "ACB",
                        null));


        cases.add(
                new Case(
                        convertToNodes(
                                "    +--B--+\n" +
                                        "    |   +-C-+\n" +
                                        " @--A-+ | | |\n" +
                                        "    | | +-+ D\n" +
                                        "    +-+     |\n" +
                                        "            x"),
                        "@--A-+|+-+|A|+--B--+C|+-+|+-C-+|D|x",
                        "ABCD",
                        null));


        cases.add(
                new Case(
                        convertToNodes(
                                " +-B-+\n" +
                                        " |  +C-+\n" +
                                        "@A+ ++ D\n" +
                                        " ++    x"),
                        "@A+++A|+-B-+C+++C-+Dx",
                        "ABCD",
                        null));


        cases.add(
                new Case(
                        convertToNodes(
                                "     -A---+\n" +
                                        "          |\n" +
                                        "  x-B-+   C\n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        null,
                        null,
                        "No start"
                ));


        cases.add(
                new Case(
                        convertToNodes(
                                "   @--A---+\n" +
                                        "          |\n" +
                                        "    B-+   C\n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        null,
                        null,
                        "No end"));


        cases.add(
                new Case(
                        convertToNodes(
                                "@--A-@-+\n" +
                                        "          |\n" +
                                        "  x-B-+   C\n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        null,
                        null,
                        "Multiple starts"));


        cases.add(
                new Case(
                        convertToNodes(
                                "   @--A---+\n" +
                                        "          |\n" +
                                        "  x-Bx+   C\n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        null,
                        null,
                        "Multiple ends"));


        cases.add(
                new Case(
                        convertToNodes(
                                "        x-B\n" +
                                        "          |\n" +
                                        "   @--A---+\n" +
                                        "          |\n" +
                                        "     x+   C\n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        null,
                        null,
                        "T fork"));

        cases.add(
                new Case(
                        convertToNodes(
                                "   @--A---+\n" +
                                        "          |\n" +
                                        "     x+    \n" +
                                        "      |   |\n" +
                                        "      +---+"),
                        null,
                        null,
                        "Broken path"));

        cases.add(
                new Case(
                        convertToNodes("   @ -A---x\n"),
                        null,
                        null,
                        "Broken path"));

        for (Case caze : cases) {
            connectNodes(caze.getNodes());
        }
    }

    private static List<Node> convertToNodes(String input) {
        var lines = input.split("\n");
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            var line = lines[i];
            for (int j = 0; j < line.length(); j++) {
                char value = line.charAt(j);
                if (value != ' ') {
                    Node node = new Node(value, j, i);
                    nodes.add(node);
                }
            }
        }
        return nodes;
    }

    static void connectNodes(List<Node> nodes) {
        for (Node currentNode : nodes) {
            currentNode.setUp(
                    nodes
                            .stream()
                            .filter(node -> node.getX() == currentNode.getX() && node.getY() == currentNode.getY() - 1)
                            .findAny()
                            .orElse(null));

            currentNode.setDown(
                    nodes
                            .stream()
                            .filter(node -> node.getX() == currentNode.getX() && node.getY() == currentNode.getY() + 1)
                            .findAny()
                            .orElse(null));

            currentNode.setLeft(
                    nodes
                            .stream()
                            .filter(node -> node.getX() == currentNode.getX() - 1 && node.getY() == currentNode.getY())
                            .findAny()
                            .orElse(null));

            currentNode.setRight(
                    nodes
                            .stream()
                            .filter(node -> node.getX() == currentNode.getX() + 1 && node.getY() == currentNode.getY())
                            .findAny()
                            .orElse(null));
        }
    }
}
