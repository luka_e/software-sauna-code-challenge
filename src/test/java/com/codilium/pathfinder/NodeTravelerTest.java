package com.codilium.pathfinder;

import com.codilium.pathfinder.helper.DirectionHelper;
import com.codilium.pathfinder.helper.NodeHelper;
import com.codilium.pathfinder.model.Case;
import com.codilium.pathfinder.model.Result;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NodeTravelerTest {

    private NodeTraveler nodeTraveler;

    @Before
    public void setUp() {
        this.nodeTraveler = new NodeTraveler(new NodeHelper(), new DirectionHelper());
    }

    @Test
    public void passesAllCases() {
        for(Case caze : Inputs.cases) {
            try {
                Result result = nodeTraveler.travelNodes(caze.getNodes());
                Assert.assertEquals(caze.getResultLetters(), result.getLetters());
                Assert.assertEquals(caze.getResultPath(), result.getPath());
            } catch (Exception e) {
                Assert.assertEquals(caze.getError(), e.getMessage());
            }
        }
    }
}
