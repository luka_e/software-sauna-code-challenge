package com.codilium.pathfinder.model;

import com.codilium.pathfinder.enums.Direction;
import org.junit.Assert;
import org.junit.Test;

public class NodeTest {

    @Test
    public void findsNodeForDirection() {
        Node node = new Node('A', 5, 5);
        Node right = new Node('-', 6, 5);
        node.setRight(right);
        Node left = new Node('-', 4, 5);
        node.setLeft(left);
        Node up = new Node('|', 5, 4);
        node.setUp(up);
        Node down = new Node('|', 5, 6);
        node.setDown(down);
        Assert.assertEquals(right, node.getNodeForDirection(Direction.RIGHT));
        Assert.assertEquals(left, node.getNodeForDirection(Direction.LEFT));
        Assert.assertEquals(up, node.getNodeForDirection(Direction.UP));
        Assert.assertEquals(down, node.getNodeForDirection(Direction.DOWN));
    }

    @Test
    public void returnsNullWhenNodeIsNotPresent() {
        Node node = new Node('A', 5, 5);
        Assert.assertNull(node.getNodeForDirection(Direction.RIGHT));
        Assert.assertNull(node.getNodeForDirection(Direction.LEFT));
        Assert.assertNull(node.getNodeForDirection(Direction.UP));
        Assert.assertNull(node.getNodeForDirection(Direction.DOWN));
    }
}
