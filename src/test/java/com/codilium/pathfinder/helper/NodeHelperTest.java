package com.codilium.pathfinder.helper;

import com.codilium.pathfinder.exception.*;
import com.codilium.pathfinder.model.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class NodeHelperTest {

    private NodeHelper nodeHelper;

    @Before
    public void setUp() {
        this.nodeHelper = new NodeHelper();
    }

    @Test
    public void detectsLetter() {
        for(char c = 'A'; c <= 'Z'; c++) {
            Assert.assertTrue(this.nodeHelper.isLetter(new Node(c, 0, 0)));
        }
    }

    @Test
    public void detectsNonLetter() {
        char[] symbols = new char[]{'@', '|', '-', 'x'};
        for(char c : symbols) {
            Assert.assertFalse(this.nodeHelper.isLetter(new Node(c, 0, 0)));
        }
    }

    @Test(expected = TForkException.class)
    public void detectsValidTFork() {
        Node turn = new Node('+', 5, 5);
        turn.setUp(new Node('|', 5, 4));
        turn.setDown(new Node('|', 5, 6));
        turn.setRight(new Node('-', 6, 5));
        this.nodeHelper.validateTurn(turn);
    }

    @Test
    public void passesInvalidTForkWithFalseSideNode() {
        Node turn = new Node('+', 5, 5);
        turn.setUp(new Node('|', 5, 4));
        turn.setDown(new Node('|', 5, 6));
        turn.setRight(new Node('|', 6, 5));
        this.nodeHelper.validateTurn(turn);
    }

    @Test
    public void passesInvalidTForkWithFalseVerticalNode() {
        Node turn = new Node('+', 5, 5);
        turn.setUp(new Node('-', 5, 4));
        turn.setDown(new Node('|', 5, 6));
        turn.setRight(new Node('-', 6, 5));
        this.nodeHelper.validateTurn(turn);
    }

    @Test
    public void passesValidTurn() {
        Node turn = new Node('+', 5, 5);
        turn.setDown(new Node('|', 5, 6));
        turn.setRight(new Node('-', 6, 5));
        this.nodeHelper.validateTurn(turn);
    }

    @Test
    public void findsStart() {
        List<Node> nodes = List.of(
            new Node('@', 0, 0),
            new Node('-', 1, 0),
            new Node('-', 2, 0),
            new Node('x', 3, 0));
        Node start = this.nodeHelper.findStart(nodes);
        Assert.assertNotNull(start);
        Assert.assertEquals('@', start.getValue());
    }

    @Test(expected = NoStartException.class)
    public void detectsNoStart() {
        List<Node> nodes = List.of(
                new Node('-', 1, 0),
                new Node('-', 2, 0),
                new Node('x', 3, 0));
        this.nodeHelper.findStart(nodes);
    }

    @Test(expected = MultipleStartsException.class)
    public void detectsMultipleStarts() {
        List<Node> nodes = List.of(
                new Node('@', 0, 0),
                new Node('@', 1, 0),
                new Node('-', 2, 0),
                new Node('x', 3, 0));
        this.nodeHelper.findStart(nodes);
    }

    @Test
    public void passesValidEnd() {
        List<Node> nodes = List.of(
                new Node('@', 0, 0),
                new Node('-', 1, 0),
                new Node('-', 2, 0),
                new Node('x', 3, 0));
        this.nodeHelper.validateEnd(nodes);
    }

    @Test(expected = NoEndException.class)
    public void detectsNoEnd() {
        List<Node> nodes = List.of(
                new Node('@', 0, 0),
                new Node('-', 1, 0),
                new Node('-', 2, 0));
        this.nodeHelper.validateEnd(nodes);
    }

    @Test(expected = MultipleEndsException.class)
    public void detectsMultipleEnds() {
        List<Node> nodes = List.of(
                new Node('@', 0, 0),
                new Node('-', 1, 0),
                new Node('x', 2, 0),
                new Node('x', 3, 0));
        this.nodeHelper.validateEnd(nodes);
    }
}
