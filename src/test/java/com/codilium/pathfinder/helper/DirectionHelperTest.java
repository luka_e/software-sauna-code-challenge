package com.codilium.pathfinder.helper;

import com.codilium.pathfinder.enums.Direction;
import com.codilium.pathfinder.exception.BrokenPathException;
import com.codilium.pathfinder.model.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DirectionHelperTest {

    private DirectionHelper directionHelper;

    @Before
    public void setUp() {
        this.directionHelper = new DirectionHelper();
    }

    @Test
    public void findsInitialDirection() {
        Node start = new Node('@', 0, 0);
        Node next = new Node('-', 1, 0);
        start.setRight(next);
        Assert.assertEquals(Direction.RIGHT, directionHelper.findInitialDirection(start));
    }

    @Test(expected = BrokenPathException.class)
    public void throwsWhenStartNotConnected() {
        Node start = new Node('@', 0, 0);
        directionHelper.findInitialDirection(start);
    }

    @Test
    public void findsTurnDirection() {
        Node turn = new Node('+', 5, 5);
        turn.setUp(new Node('|', 5, 4));
        turn.setRight(new Node('-', 6, 5));
        Assert.assertEquals(Direction.RIGHT, directionHelper.determineDirection(turn, Direction.DOWN));
    }
}
