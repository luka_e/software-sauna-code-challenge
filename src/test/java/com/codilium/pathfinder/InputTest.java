package com.codilium.pathfinder;

import com.codilium.pathfinder.model.Node;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class InputTest {

    @Test
    public void connectsVerticallyAdjacentNodes() {
        Node leftNode = new Node('-', 0, 0);
        Node rightNode = new Node('-', 1, 0);
        List<Node> nodes = List.of(leftNode, rightNode);
        Inputs.connectNodes(nodes);

        Assert.assertEquals(leftNode, rightNode.getLeft());
        Assert.assertEquals(rightNode, leftNode.getRight());
    }

    @Test
    public void connectsHorizontallyAdjacentNodes() {
        Node upperNode = new Node('|', 0, 0);
        Node lowerNode = new Node('|', 0, 1);
        List<Node> nodes = List.of(upperNode, lowerNode);
        Inputs.connectNodes(nodes);

        Assert.assertEquals(upperNode, lowerNode.getUp());
        Assert.assertEquals(lowerNode, upperNode.getDown());
    }
}
